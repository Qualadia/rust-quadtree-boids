use crate::utils::divide_rect;

use bevy::{
    prelude::Event,
    math::{
        Vec2,
        Rect
    }
};

pub struct QuadNode {
    rect: Rect,
    children_quads: Option<Box<[QuadNode; 4]>>,
    children_boids: Option<Vec<i32>>
}

#[derive(Event)]
pub struct QuadTree {
    pub root: Box<QuadNode>
}

impl QuadNode {
    pub fn new(boids: Vec<(i32, Vec2)>, rect: Rect, max_boids_in_node: Option<usize>) -> QuadNode {
        let max_boids_in_node_unwrapped: usize = max_boids_in_node.unwrap_or(4);
        let contained: Vec<_> = boids
            .into_iter()
            .filter(|e| rect.contains(e.1))
            .collect();
        if contained.len() > max_boids_in_node_unwrapped {
            let divided: [Rect; 4] = divide_rect(rect);
            let mut tl = Vec::new();
            let mut tr = Vec::new();
            let mut dl = Vec::new();
            let mut dr = Vec::new();
            for i in contained {
                if divided[0].contains(i.1) {
                    tl.push(i);
                } else if divided[1].contains(i.1) {
                    tr.push(i);
                } else if divided[2].contains(i.1) {
                    dl.push(i);
                } else if divided[3].contains(i.1) {
                    dr.push(i);
                } else {
                        panic!("Weird!!!!");
                }
            }
            return QuadNode {
                rect,
                children_quads: 
                    Some(Box::new([
                        QuadNode::new(
                            tl,
                            divided[0],
                            Some(max_boids_in_node_unwrapped)
                        ),
                        QuadNode::new(
                            tr,
                            divided[1],
                            Some(max_boids_in_node_unwrapped)
                        ),
                        QuadNode::new(
                            dl,
                            divided[2],
                            Some(max_boids_in_node_unwrapped)
                        ),
                        QuadNode::new(
                            dr,
                            divided[3],
                            Some(max_boids_in_node_unwrapped)
                        )
                    ])),
                children_boids: None
            }
        } else {
            let ids: Vec<i32> = contained
                .into_iter()
                .map(|e| e.0)
                .collect();
            return QuadNode{
                rect,
                children_quads: None,
                children_boids: Some(ids)
            };
        }
    }


    // TODO: refactor
    pub fn collect_contained_elems(&self) -> Vec<& i32> {
        if let Some(quads) = &self.children_quads {
            let mut res = Vec::new();
            quads
                .iter()
                .map(|e| res.append(&mut e.collect_contained_elems()));
            return res;
        } else {
            let childs = self.children_boids.as_ref().unwrap();
            let collected: Vec<_> = childs
                    .iter()
                    .collect();
            return collected;
        }
    }

    pub fn node_contains_rect(&self, rect: Rect) -> Vec<&QuadNode> {
        if self.rect.intersect(rect).is_empty() { return Vec::new()};

        if let Some(quads) = &self.children_quads {
            let mut contained: Vec<&QuadNode> = Vec::new();
            for i in quads.iter() {
                let mut res = i.node_contains_rect(rect);
                contained.append(&mut res);
            }
            return contained;
        } else {
            return vec![self];
        }
    }
    
    pub fn node_contains_id(&self, id: i32) -> Option<&QuadNode> {
        if let Some(quads) = &self.children_quads {
            for i in quads.iter() {
                if i.node_contains_id(id).is_some() {
                    return Some(i);
                }
            }
            return None;
        } else {
            for i in self.children_boids.as_ref().unwrap() {
                if *i == id {
                    return Some(self);
                }
            }
            return None;
        }
    }
    
    pub fn find_near_elems(&self, id: i32, radius: f32) -> Vec<& i32> {
        if let Some(node) = self.node_contains_id(id) {
            let soe = Rect::from_center_size(node.rect.center(), Vec2::new(radius*2., radius*2.));
            let nodes = self.node_contains_rect(soe);
            let some: Vec<_> = nodes
                .iter()
                .map(|e| e.collect_contained_elems())
                .flatten()
                .collect();
            return some;
        } else {
            return Vec::new();
        }
    }

    pub fn collect_rects(&self) -> Vec<Rect> {
        let mut rects: Vec<Rect> = vec![self.rect];
        if let Some(children) = &self.children_quads {
            for i in children.iter() {
                rects.extend(i.collect_rects());
            }
        }
        return rects;
    }
}

