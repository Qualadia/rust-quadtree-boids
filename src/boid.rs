use bevy::{
    prelude::*,
    math::Vec2
};

use bevy::utils::HashMap;

use rand::Rng;
use crate::QuadTree;

use crate::{
    BOIDS_SPEED,
    BOIDS_BEHAVIOUR_RADIUS,
    MIN_STATE_TIME,
    MAX_STATE_TIME
};

#[derive(Component)]
pub struct Boid {
    pub current_state: BoidState,
    pub time_until_update: f32
}

#[derive(Component)]
pub struct Id {
    pub id: i32
}

#[derive(Component)]
pub struct Velocity {
    pub direction_rad: f32,
    pub amount: f32
}

#[derive(Component)]
pub struct Movable {
    pub wanted_move: Vec2
}

pub enum BoidState {
    Separation,
    Aligment,
    Cohesion
}

impl BoidState {
    fn choose_random_state(&self) -> BoidState {
        let mut rng = rand::thread_rng();
        let rand: i32 = rng.gen_range(0..=2);
        match rand {
            0 => BoidState::Separation,
            1 => BoidState::Aligment,
            2 => BoidState::Cohesion,
            _ => {
                panic!("Weird! Rand return int that's not 0, 1 or 2.")
            }
        }
    }
}

pub fn boid_update(
    mut boids: Query<(&mut Transform, &mut Boid, &mut Movable, &mut Velocity, &Id)>,
    mut quadtree: EventReader<QuadTree>,
    time_step: Res<FixedTime>,
) {
    if quadtree.is_empty() {return;}
    let quadtree_read: Vec<_> = quadtree
        .into_iter()
        .map(|e| &e.root)
        .collect();
    let current_tree = quadtree_read.last().unwrap();
    let lookup_table: HashMap<i32, Vec2> = boids
        .iter()
        .fold(HashMap::new(), |mut sum, val| {
            sum.insert(val.4.id, Vec2::new(val.0.translation.x, val.0.translation.y)); sum
        });
    for mut props in boids.iter_mut() {
        props.1.time_until_update -= time_step.period.as_secs_f32();

        if props.1.time_until_update <= 0.0 {
            props.1.current_state = props.1.current_state.choose_random_state();
            let mut rng = rand::thread_rng();
            props.1.time_until_update = rng.gen_range(MIN_STATE_TIME..MAX_STATE_TIME);
        }
        match props.1.current_state {
            BoidState::Separation => {
                let near_boids = current_tree.find_near_elems(props.4.id, BOIDS_BEHAVIOUR_RADIUS);
                let vector: Vec2 = near_boids
                    .iter()
                    .filter_map(|e| {
                        let diff = lookup_table.get(*e).unwrap().clone() - Vec2::new(props.0.translation.x, props.0.translation.y);
                        match Vec2::ZERO.distance(diff) <= BOIDS_BEHAVIOUR_RADIUS {
                            true => Some(diff),
                            false => None
                        }
                    })
                    .sum();
                if vector == Vec2::ZERO {
                    props.2.wanted_move = Vec2::new(props.3.direction_rad.cos(), props.3.direction_rad.sin()) * BOIDS_SPEED;
                    continue;
                }
                props.3.direction_rad += (-vector).angle_between(Vec2::new(props.3.direction_rad.cos(), props.3.direction_rad.sin())) * 0.2;
                props.2.wanted_move = Vec2::new(props.3.direction_rad.cos(), props.3.direction_rad.sin()) * BOIDS_SPEED;
            },
            BoidState::Aligment => {
                // props.2.wanted_move = Vec2::new(0., 0.);
            },
            BoidState::Cohesion => {
                let near_boids = current_tree.find_near_elems(props.4.id, BOIDS_BEHAVIOUR_RADIUS);
                let vector: Vec2 = near_boids
                    .iter()
                    .filter_map(|e| {
                        let diff = lookup_table.get(*e).unwrap().clone() - Vec2::new(props.0.translation.x, props.0.translation.y);
                        match Vec2::ZERO.distance(diff) <= BOIDS_BEHAVIOUR_RADIUS {
                            true => Some(diff),
                            false => None
                        }
                    })
                    .sum();
                if vector == Vec2::ZERO {
                    props.2.wanted_move = Vec2::new(props.3.direction_rad.cos(), props.3.direction_rad.sin()) * BOIDS_SPEED;
                    continue;
                }
                props.3.direction_rad += vector.angle_between(Vec2::new(props.3.direction_rad.cos(), props.3.direction_rad.sin())) * 0.2;
                props.2.wanted_move = Vec2::new(props.3.direction_rad.cos(), props.3.direction_rad.sin()) * BOIDS_SPEED;
            }
        }
    }
}
