mod quadtree;
mod utils;
mod boid;
mod systems;

use self::quadtree::QuadTree;
use self::boid::*;
use systems::*;

use bevy::{
    prelude::*,
    diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin},
    window::{CursorGrabMode, PresentMode, WindowLevel, WindowTheme}
};

pub const BOIDS_COUNT: u32 = 100;
pub const BOIDS_RADIUS: f32 = 3.0;
pub const BOIDS_BEHAVIOUR_RADIUS: f32 = 100.0;
pub const BOIDS_SPEED: f32 = 150.0;

pub const MIN_STATE_TIME: f32 = 0.5;
pub const MAX_STATE_TIME: f32 = 1.;

pub const MAX_BOIDS_IN_NODE: usize = 4;

fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins.set(WindowPlugin {
                primary_window: Some(Window {
                    title: "I am a window!".into(),
                    resolution: (600., 600.).into(),
                    present_mode: PresentMode::AutoVsync,
                    // Tells wasm to resize the window according to the available canvas
                    resizable: false,
                    fit_canvas_to_parent: true,
                    // Tells wasm not to override default event handling, like F5, Ctrl+R etc.
                    prevent_default_event_handling: false,
                    window_theme: Some(WindowTheme::Dark),
                    // enabled_buttons: bevy::window::EnabledButtons {
                        // maximize: false,
                        // ..Default::default()
                    // },
                    ..default()
                }),
                ..default()
            }),
            LogDiagnosticsPlugin::default(),
            FrameTimeDiagnosticsPlugin,
        ))
        .add_event::<QuadTree>()
        .add_systems(Startup, setup)
        .add_systems(Update, quadtree_creation)
        .add_systems(Update, boid_update)
        .add_systems(Update, collision_compute)
        .add_systems(Update, render_quadtree)
        .run();
}
