use bevy::math::Rect;

pub fn divide_rect(rect: Rect) -> [Rect; 4] {
    [
        Rect::new(
            rect.min.x,
            rect.min.y,
            rect.min.x + rect.width()/2.0,
            rect.min.y + rect.height()/2.0
        ),
        Rect::new(
            rect.min.x + rect.width()/2.0,
            rect.min.y,
            rect.max.x,
            rect.min.y + rect.height()/2.0
        ),
        Rect::new(
            rect.min.x,
            rect.min.y + rect.height()/2.0,
            rect.min.x + rect.width()/2.0,
            rect.max.y
        ),
        Rect::new(
            rect.min.x + rect.width()/2.0,
            rect.min.y + rect.height()/2.0,
            rect.max.x,
            rect.max.y
        )
    ]
}
