use rand::Rng;
use bevy::{
    prelude::*,
    sprite::MaterialMesh2dBundle,
    window::PrimaryWindow,
    math::Vec2,
    math::Rect,
    ecs::prelude::Query
};
use crate::{
    BOIDS_COUNT,
    BOIDS_RADIUS,
    MIN_STATE_TIME,
    MAX_STATE_TIME,
    MAX_BOIDS_IN_NODE,
    BOIDS_BEHAVIOUR_RADIUS
};
use crate::boid::*;
use crate::quadtree::{
    QuadTree,
    QuadNode
};
use std::f32::consts::PI;

pub fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    window_query: Query<&Window, With<PrimaryWindow>>
) {
    commands.spawn(Camera2dBundle::default());
    let window: &Window = window_query.single();
    let width: f32 = window.width();
    let height: f32 = window.height();
    let mut rng = rand::thread_rng();
    for _i in 0..=BOIDS_COUNT {
        let posx: f32 = rng.gen_range(-width/2.0..width/2.0);
        let posy: f32 = rng.gen_range(-height/2.0..height/2.0);
        commands.spawn((
        MaterialMesh2dBundle {
            mesh: meshes.add(shape::RegularPolygon::new(BOIDS_RADIUS, 3).into()).into(),
            material: materials.add(ColorMaterial::from(Color::TURQUOISE)),
            transform: Transform::from_xyz(posx, posy, 0.),
            ..default()
        },
        Boid { 
            current_state: BoidState::Cohesion,
            time_until_update: rng.gen_range(MIN_STATE_TIME..MAX_STATE_TIME),
        },
        Velocity {
            direction_rad: rng.gen_range(0.0..=2.0*PI),
            amount: rng.gen_range(0.0..=BOIDS_BEHAVIOUR_RADIUS)
        },
        Movable {
            wanted_move: Vec2{x: 0.0,y: 0.0}
        },
        Id {
            id: _i as i32
        },
    ));
    }
}

pub fn quadtree_creation(
    mut quad_tree_event: EventWriter<QuadTree>,
    boids_query: Query<(&Id, &Transform)>,
    window_query: Query<&Window, With<PrimaryWindow>>,
) {
    let rect: Rect = Rect::new(
        -window_query.single().width()/2.,
        -window_query.single().height()/2.,
        window_query.single().width()/2.,
        window_query.single().height()/2.
    );
    let collected: Vec<(i32, Vec2)> = boids_query
        .iter()
        .map(|e| (
            e.0.id, 
            Vec2::new(
                e.1.translation.x,
                e.1.translation.y)
        )).collect();
    let tree = QuadNode::new(collected, rect, Some(MAX_BOIDS_IN_NODE));
    quad_tree_event.send( QuadTree {
        root: Box::new(tree)
    })
}

pub fn collision_compute(
    mut boids: Query<(&mut Transform, &mut Boid, &mut Movable, &mut Velocity, &Id)>,
    time_step: Res<FixedTime>,
    window_query: Query<&Window, With<PrimaryWindow>>,
) {
    let rect: Rect = Rect::new(
        -window_query.single().width()/2.,
        -window_query.single().height()/2.,
        window_query.single().width()/2.,
        window_query.single().height()/2.
    );
    let len: i32 = boids
        .iter()
        .map(|_e| 1)
        .sum();
    for mut props in boids.iter_mut() {
        let mut new_translation: Vec3 = props.0.translation + Vec3::new(props.2.wanted_move.x, props.2.wanted_move.y, 0.) * time_step.period.as_secs_f32();
        if rect.max.x < new_translation.x {
            new_translation.x -= new_translation.x - rect.max.x + 15.;
        } else if rect.min.x > new_translation.x {
            new_translation.x += rect.min.x - new_translation.x + 15.;
        }
        if rect.max.y < new_translation.y {
            new_translation.y -= new_translation.y - rect.max.y + 15.;
        } else if rect.min.y > new_translation.y {
            new_translation.y += rect.min.y - new_translation.y + 15.;
        }
        props.0.translation = new_translation;
    }
}

pub fn render_quadtree(
    mut quadtree: EventReader<QuadTree>,
    mut gizmos: Gizmos,
) {
    if quadtree.is_empty() {return;}
    let quadtree_read = quadtree
        .into_iter()
        .map(|e| &e.root)
        .collect::<Vec<_>>();
    let quad = quadtree_read.last().unwrap();
    let rects: Vec<Rect> = quad.collect_rects();
    for i in rects.iter() {
        gizmos.rect_2d(
                i.center(), 
                0., 
                i.size(), 
                Color::GREEN)
    }
}
