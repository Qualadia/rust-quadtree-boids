{
  description = "boids_quadtree";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = {self, nixpkgs, flake-utils, ...}: flake-utils.lib.eachDefaultSystem(system: 
    let
      pkgs = import nixpkgs {
        inherit system;
      };
      in {
        packages = {
          default = pkgs.callPackage(
            {
              stdenv,
              lib,
            }: pkgs.rustPlatform.buildRustPackage {
              name = "boids_quadtree";
              pname = "boids_quadtree";
              src = ./.;
              cargoHash = "sha256-E7tHKww2bN/jh3w0uzOWRYJYTFkk19B5EckGTnLI628=";
            }
          ) {};
        };
        devShell =
          let  
          deps = with pkgs; [
            udev alsa-lib vulkan-loader
            xorg.libX11 xorg.libXcursor xorg.libXi xorg.libXrandr # To use the x11 feature
            libxkbcommon wayland # To use the wayland feature            
          ];
          in pkgs.mkShell {
          nativeBuildInputs = with pkgs; [
            cargo
            rustc
            rustup
            pkg-config
          ];
          buildInputs = deps;
          LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath (deps);
        };
      }
    );
  
}

